fn median_of_three(list: &Vec<impl PartialOrd>, left: usize, right: usize) -> usize {
    let half_index = (right - left) / 2 + left;
    let beginning = &list[left];
    let middle = &list[half_index];
    let end = &list[right];
    let comparison =
        |median, other| median >= other && median <= end || median >= end && median <= other;
    if comparison(beginning, middle) {
        left
    } else if comparison(middle, beginning) {
        half_index
    } else {
        right
    }
}

// hoare's partition scheme
fn partition(
    list: &mut Vec<impl PartialOrd>,
    mut left: usize,
    mut right: usize,
    pivot_index: usize,
) -> usize {
    while &list[left] < &list[pivot_index] {
        left += 1
    }
    while &list[right] > &list[pivot_index] {
        right -= 1
    }
    if left < right {
        list.swap(left, right);
        right -= 1; // no do while so put extra operations here
        left += 1;
        partition(list, left, right, pivot_index)
    } else {
        left
    }
}

/*// lomunto partition scheme
fn partition(
    list: &mut Vec<impl PartialOrd>,
    left: usize,
    right: usize,
    pivot_index: usize,
) -> usize {
    list.swap(pivot_index, right);

    let mut store_index = left;
    for i in left..right {
        if list[i] < list[right] {
            list.swap(i, store_index);
            store_index += 1
        }
    }
    list.swap(right, store_index);
    store_index
}*/


fn select<T: PartialOrd>(list: &mut Vec<T>, left: usize, right: usize, k: usize) -> &T {
    if left != right {
        let pivot_index = partition(list, left, right, median_of_three(list, left, right));
        if k == pivot_index {
            &list[k]
        } else if k < pivot_index {
            select(list, left, pivot_index - 1, k)
        } else {
            select(list, pivot_index + 1, right, k)
        }
    } else {
        &list[left]
    }
}

/// Run quick select on vector to find kth smallest (0 based)
pub fn quick_select<T: PartialOrd>(array: &mut Vec<T>, k: usize) -> &T {
    select(array, 0, array.len() - 1, k)
}

#[cfg(test)]
mod tests {
    use crate::{partition, quick_select, select};

    #[test]
    fn partition_test() {
        let mut vector = vec![1, 1, -43, 0, -1, -1, -1];
        let len = vector.len() - 1;
        let result = partition(&mut vector, 0, len, 3);
        assert_eq!(vector, vec![-1, -1, -43, -1, 0, 1, 1]);
        assert_eq!(result, 4)
    }

    #[test]
    fn select_test() {
        let mut vector = vec![3, 2, 1, 6, 5, 4, 0];
        let len = vector.len() - 1;
        for i in 0..len {
            let result = *select(&mut vector, 0, len, i);
            dbg!(&vector);
            assert_eq!(result, i)
        }
    }

    #[test]
    fn quick_select_test() {
        let mut vector = vec![3, 2, 1, 6, 5, 4, 0];
        assert_eq!(*quick_select(&mut vector, 0), 0);
    }

    #[test]
    fn bench() {
        let mut vector = vec![100, 2, 7, 1, 4, 43, 0];
        for _i in 0..100_000 {
            for i in 0..vector.len() {
                quick_select(&mut vector, i);
            }
        }
    }
}
